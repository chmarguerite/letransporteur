package com.kannacorp.letransporteur.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.kannacorp.letransporteur.model.Transporteur;
import com.kannacorp.letransporteur.domaine.TransporteurRepositoryPort;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import com.kannacorp.letransporteur.model.*;

@Repository
@Qualifier("JpaRepository")
public class TransporteurRepositoryAdapter implements TransporteurRepositoryPort {
	ModelMapper converter = new ModelMapper();
	private TransporteurRepository transporteurRepository;

	public TransporteurRepositoryAdapter(TransporteurRepository transporteurRepository){
		this.transporteurRepository = transporteurRepository;
	}


	public List<Transporteur> obtenirTransporteurs() {
		List<Transporteur> listeTransporteurs = new ArrayList<Transporteur>();
		Iterator<Transporteur> iterator= transporteurRepository.findAll().iterator();
		while(iterator.hasNext()){
			listeTransporteurs.add(converter.map(iterator.next(), Transporteur.class));
		}
		return listeTransporteurs;
	}

	public Transporteur ajouterTransporteur(Transporteur pTransporteur) {

		Transporteur t = transporteurRepository.save(converter.map(pTransporteur, Transporteur.class));
		t.setCode(genererIdTransporteur(t.getId()));
		transporteurRepository.save(t);
		pTransporteur.setCode(t.getCode());
		return pTransporteur;
	}

	public Transporteur obtenirTransporteurParCode(String pCode) {
		Transporteur t = transporteurRepository.trouverParCode(pCode);
		 if( t!= null){
		 	return converter.map(t, Transporteur.class);
		 }else {
			 return null;
		 }
	}


	public Transporteur modifierTransporteur(Transporteur pTransporteur) {
		Transporteur t = transporteurRepository.trouverParCode(pTransporteur.getCode());
		BeanUtils.copyProperties(pTransporteur,t);
		transporteurRepository.save(t);
		return converter.map(t, Transporteur.class);
	}


	public void supprimerTransporteur(String code) {
		transporteurRepository.supprimerTransporteur(code);
	}


	public String genererIdTransporteur(Long id) {
		return "TRAN-" + id;
	}


/*
	public void supprimerTransporteur(long pIdTransporteur) {
		transporteurRepository.supprimerTransporteur(pIdTransporteur);
	}

	public Transporteur obtenirTransporteur(long pIdTransporteur) {
		return transporteurRepository.getOne(pIdTransporteur).toTransporteur();	
	}


	public void modifierTransporteur(long pIdTransporteur, Transporteur pTransporteur) {
		TransporteurEntity transporteurEntity = transporteurRepository.getOne(pIdTransporteur);
		transporteurEntity.setAdresse(pTransporteur.getAdresse());
		transporteurEntity.setApeNaf(pTransporteur.getApeNaf());
		transporteurEntity.setCode(pTransporteur.getCode());
		transporteurEntity.setEmail(pTransporteur.getEmail());
		transporteurEntity.setNom(pTransporteur.getNom());
		transporteurEntity.setPortable(pTransporteur.getPortable());
		transporteurEntity.setPrenom(pTransporteur.getPrenom());
		transporteurEntity.setRc(pTransporteur.getRc());
		transporteurEntity.setSiren(pTransporteur.getSiren());
		transporteurEntity.setSiret(pTransporteur.getSiret());
		transporteurEntity.setTelephone(pTransporteur.getTelephone());
		transporteurEntity.setTelecopie(pTransporteur.getTelecopie());
		transporteurRepository.save(transporteurEntity);
	}

*/

}
