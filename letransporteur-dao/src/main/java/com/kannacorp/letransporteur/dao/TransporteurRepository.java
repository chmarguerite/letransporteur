package com.kannacorp.letransporteur.dao;

import com.kannacorp.letransporteur.domaine.Transporteur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

public interface TransporteurRepository extends JpaRepository<Transporteur, Long> {

    @Query("SELECT t FROM TransporteurEntity t where t.code = :code")
    Transporteur trouverParCode(@Param("code") String code);

   @Modifying
    @Transactional
    @Query("delete from TransporteurEntity t where t.code= :code")
    void supprimerTransporteur(@Param("code") String code);

}
