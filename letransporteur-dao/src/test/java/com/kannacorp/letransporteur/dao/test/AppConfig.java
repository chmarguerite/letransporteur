package com.kannacorp.letransporteur.dao.test;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan(basePackages = {"com.kannacorp.letransporteur" })
@EntityScan("com.kannacorp.letransporteur.dao")
@EnableJpaRepositories("com.kannacorp.letransporteur.dao")
public class AppConfig {
}
