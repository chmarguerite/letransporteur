package com.kannacorp.letransporteur.dao.test.tu;

import com.kannacorp.letransporteur.dao.test.AppConfig;
import com.kannacorp.letransporteur.domaine.TransporteurGestionnaire;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import com.kannacorp.letransporteur.model.Transporteur;
import com.kannacorp.letransporteur.domaine.TransporteurRepositoryPort;
import static org.assertj.core.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = AppConfig.class)
@DataJpaTest
public class TransporteurTests {
	
	@Autowired
	@Qualifier("JpaRepository")
	private TransporteurRepositoryPort transporteurRepositoryPort;
	
	@Test
	@Sql({"/test-schema.sql"})
	public void test(){
		TransporteurGestionnaire transporteurMetier = new TransporteurGestionnaire(transporteurRepositoryPort);
		System.out.println("Début du test des fonctionnalités de gestion du Transporteur");
		assertThat(transporteurMetier.obtenirListeTransporteurs()).hasSize(0);
		/*transporteurMetier.creerTransporteur("CODE", "NOM", "PRENOM", "010000000", "06000000", "000-000", "RUE DES 3 CHANDELLES 97280 LE VAUCLIN" , "chmarguerite@transporteur.mq", "8235469878001", "823564874", "620Z", "YYTY778");
		assertThat(transporteurMetier.obtenirTransporteurs()).hasSize(1);
		transporteurMetier.creerTransporteur("CODE2", "NOM2", "PRENOM2", "010000000", "06000000", "000-000", "RUE DES 3 CHANDELLES 97280 LE VAUCLIN" , "chmarguerite@transporteur.mq", "8235469878001", "823564874", "620Z", "YYTY778");
		assertThat(transporteurMetier.obtenirTransporteurs()).hasSize(2);
		transporteurMetier.supprimerTransporteur((long) 1);
		assertThat(transporteurMetier.obtenirTransporteurs()).hasSize(1);
		Transporteur t = transporteurMetier.consulterTransporteur((long) 2);
		assertThat(t).isNotNull();
		assertThat(t.getCode()).isEqualTo("CODE2");
		assertThat(t.getSiren()).isEqualTo("823564874");
		transporteurMetier.modifierTransporteur(2, "CODE2M", "NOM2", "PRENOM2", "010000000", "06000000", "000-000", "28 RUE PIERROT 93200 ST-DENIS" , "chmarguerite@transporteur.mq", "8235469878001", "111111111", "620Z", "YYTY778");
		assertThat(transporteurMetier.obtenirTransporteurs()).hasSize(1);
		Transporteur t1 = transporteurMetier.consulterTransporteur((long) 2);
		assertThat(t1.getCode()).isEqualTo("CODE2M");
		assertThat(t1.getSiren()).isEqualTo("111111111");
		assertThat(t1.getAdresse()).isEqualTo("28 RUE PIERROT 93200 ST-DENIS");*/
		
		System.out.println("Fin du test des fonctionnalités de gestion du Transporteur");
		
	}
}
