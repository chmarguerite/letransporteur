package com.kannacorp.letransporteur.dao.test.ti;

import com.kannacorp.letransporteur.dao.test.AppConfig;

import com.kannacorp.letransporteur.domaine.Transporteur;
import com.kannacorp.letransporteur.domaine.TransporteurGestionnaire;
import com.kannacorp.letransporteur.domaine.TransporteurRepositoryPort;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.fr.Alors;
import cucumber.api.java.fr.Et;
import cucumber.api.java.fr.Etantdonné;
import cucumber.api.java.fr.Quand;
import org.apache.commons.beanutils.BeanUtils;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Java6Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = AppConfig.class)
@DataJpaTest
public class TransporteurDefinitions {
    @Autowired
    TransporteurGestionnaire transporteurGestionnaire;

    //private TransporteurRepositoryPort transporteurRepository;

    private Transporteur leTransporteur;
    @Etantdonné("je suis un administrateur")
    public void je_suis_un_administrateur() {
        assertThat(true).isTrue();
       // transporteurGestionnaire=new TransporteurGestionnaire(transporteurRepository);
    }

    @Quand("je crée un transporteur avec les informations suivantes")
    public void je_crée_un_transporteur_avec_les_informations_suivantes(List<Transporteur> lesTransporteurs) {

        leTransporteur= transporteurGestionnaire.créerUnTransporteur(lesTransporteurs.get(0));
        //Mockito.verify(transporteurRepository).ajouterTransporteur(lesTransporteurs.get(0));


    }
    @Alors("^un transporteur est créé avec le code \"([^\"]*)\"$")
    public void un_transporteur_est_créé_avec_le_code(String code) throws Exception {
        assertThat(code).isEqualTo(leTransporteur.getCode());
       // assertThat(transporteurRepository.obtenirTransporteurs()).hasSize(1);
    }


    @Etantdonné("^le transporteur \"([^\"]*)\" est enregistré$")
    public void le_transporteur_est_enregistré(String code) {
        leTransporteur = transporteurGestionnaire.obtenirTransporteur(code);
        assertThat(leTransporteur).isNotNull();
    }

    @Quand("^je recherche le transporteur \"([^\"]*)\"$")
    public void je_recherche_le_transporteur(String code) {
        leTransporteur = transporteurGestionnaire.obtenirTransporteur(code);
    }

    @Alors("^j'obtiens la fiche  transporteur suivante$")
    public void j_obtiens_la_fiche_transporteur_suivante(List<Transporteur> listeTransporteur) {
    Transporteur t = listeTransporteur.get(0);
            assertThat(leTransporteur).isEqualToIgnoringGivenFields(t, "id");
    }

    @Alors("^le transporteur \"([^\"]*)\" est modifié avec les informations suivantes$")
    public void le_transporteur_est_enregistré_avec_les_informations_suivantes(String code, List<Transporteur> transporteurs) {
        Transporteur t = transporteurs.get(0);
       assertThat(transporteurGestionnaire.obtenirTransporteur(code)).isEqualToIgnoringGivenFields(t,"id");

    }

    @Quand("^je modifie le transporteur \"([^\"]*)\" avec les informations suivantes$")
    public void je_modifie_le_transporteur_avec_les_informations_suivantes(String code, DataTable arg2) {
        Map<String,Object> valeurs=arg2.transpose().asMap(String.class,Object.class);
        try {
            BeanUtils.populate(leTransporteur,valeurs);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        leTransporteur=transporteurGestionnaire.modifierTransporteur(leTransporteur);

    }

    @Et("^il existe le transporteur \"([^\"]*)\" avec les informations suivantes$")
    public void ilExisteLeTransporteurAvecLesInformationsSuivantes(String code, List<Transporteur> transporteurs) throws Throwable {
        Transporteur t = transporteurs.get(0);
        leTransporteur = transporteurGestionnaire.obtenirTransporteur(code);
        assertThat(leTransporteur).isEqualToIgnoringGivenFields(t,"id");
    }

    @Quand("^je supprime le transporteur \"([^\"]*)\"$")
    public void jeSupprimeLeTransporteur(String code) throws Throwable {
        transporteurGestionnaire.supprimerTransporteur(code);
    }

    @Alors("^le transporteur \"([^\"]*)\" est supprimé$")
    public void leTransporteurEstSupprimé(String code) throws Throwable {
        assertThat(transporteurGestionnaire.obtenirTransporteur(code)).isNull();
    }
}
