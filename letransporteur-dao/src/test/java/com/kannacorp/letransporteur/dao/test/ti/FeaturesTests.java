package com.kannacorp.letransporteur.dao.test.ti;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber", "json:target/cucumber.json"},
        strict = true,
        features="src/test/resources/features/",
        glue = {"com.kannacorp.letransporteur.dao.test"})
public class FeaturesTests {
}
