package com.kannacorp.letransporteur.domaine.test;

import com.kannacorp.letransporteur.domaine.TransporteurGestionnaire;
import com.kannacorp.letransporteur.domaine.TransporteurRepositoryPort;
import com.kannacorp.letransporteur.model.Transporteur;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.fr.Alors;
import io.cucumber.java.fr.Etantdonné;
import io.cucumber.java.fr.Quand;
import org.apache.commons.beanutils.BeanUtils;
import org.mockito.Mockito;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import static org.assertj.core.api.Java6Assertions.assertThat;

public class TransporteurDefinitions {
    TransporteurGestionnaire transporteurGestionnaire;
    private static TransporteurRepositoryPort transporteurRepository= Mockito.mock(TransporteurRepositoryPort.class);


    private Transporteur leTransporteur;

    @Before
    public void setup(){
        transporteurGestionnaire=new TransporteurGestionnaire(transporteurRepository);

    }
    @Etantdonné("je suis un administrateur")
    public void je_suis_un_administrateur() {
        assertThat(true).isTrue();
    }

    @Quand("je crée un transporteur avec les informations suivantes")
    public void je_crée_un_transporteur_avec_les_informations_suivantes(List<Transporteur> lesTransporteurs) throws Exception {
        leTransporteur=lesTransporteurs.get(0);
        transporteurGestionnaire.créerUnTransporteur(leTransporteur);

    }
    @Alors("^un transporteur est créé avec le code \"([^\"]*)\"$")
    public void un_transporteur_est_créé_avec_le_code(String code) throws Exception {
        Mockito.verify(transporteurRepository).ajouterTransporteur(leTransporteur);
    }


    @Etantdonné("^il existe le transporteur \"([^\"]*)\" avec les informations suivantes$")
    public void il_existe_le_transporteur_avec_les_informations_suivantes(String code, List<Transporteur> transporteurs) {
        leTransporteur = transporteurs.get(0); leTransporteur.setCode(code);
        Mockito.when(transporteurRepository.obtenirTransporteurParCode(code)).thenReturn(leTransporteur);
    }
    @Alors("^le transporteur \"([^\"]*)\" est modifié avec les informations suivantes$")
    public void le_transporteur_est_enregistré_avec_les_informations_suivantes(String code, List<Transporteur> transporteurs) {
        Transporteur t = transporteurs.get(0);
       // assertThat(t).isEqualToIgnoringGivenFields(leTransporteur,"id");
        Mockito.verify(transporteurRepository).modifierTransporteur(t);
    }

    @Quand("^je modifie le transporteur \"([^\"]*)\" avec les informations suivantes$")
    public void je_modifie_le_transporteur_avec_les_informations_suivantes(String code, DataTable arg2) {
        Map<String,Object> valeurs=arg2.transpose().asMap(String.class,Object.class);
        try {
            BeanUtils.populate(leTransporteur,valeurs);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        leTransporteur=transporteurGestionnaire.modifierTransporteur(leTransporteur);

    }

    @Quand("^je supprime le transporteur \"([^\"]*)\"$")
    public void jeSupprimeLeTransporteurAvecLesInformationsSuivantes(String code) throws Throwable {
        transporteurGestionnaire.supprimerTransporteur(code);
    }

    @Alors("^le transporteur \"([^\"]*)\" est supprimé$")
    public void leTransporteurEstSupprimé(String code) throws Throwable {
        Mockito.verify(transporteurRepository).supprimerTransporteur(code);
    }

    @Etantdonné("^je ne suis pas un administrateur$")
    public void jeNeSuisPasUnAdministrateur() throws Throwable {
        System.out.println("Je ne suis pas admin");
    }

    @Alors("^le système refuse la création du transporteur$")
    public void leSystèmeRefuseLaCréationDuTransporteur() throws Throwable {
        System.out.println("Le transporteur n'est pas créé");
    }


}
