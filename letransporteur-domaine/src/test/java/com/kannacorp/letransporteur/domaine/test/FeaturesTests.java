package com.kannacorp.letransporteur.domaine.test;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber", "json:target/cucumber.json"},
        strict = true,
        features="src/test/resources/features/",
        glue = {"com.kannacorp.letransporteur.domaine.test"})
public class FeaturesTests {
}
