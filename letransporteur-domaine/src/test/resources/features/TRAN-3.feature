#language: fr

Fonctionnalité: Modification d'un transporteur
  En tant qu'administrateur,
  je peux gérer la modification d'un transporteur
  afin de gérer le référentiel des transporteurs


  Scénario: Modification d'un transporteur
    Etant donné je suis un administrateur
    Et il existe le transporteur "TRAN-1" avec les informations suivantes
     | code   |	nom 		|	prénom	|	adresse     | téléphone   | email	    | 	siret	        | 
     | TRAN-1 |	MARGUERITE 	|	Claude 	|	2 rue libre | 0637265485  | ch@trans.io	| 	82575789690023	| 
    Quand je modifie le transporteur "TRAN-1" avec les informations suivantes
      |	adresse                   | téléphone   | email	        | 	siret	        | 
      |	2 rue libre 94000 Créteil | 0715141514  | ch@trans.ifr	| 	82575789690045	| 
    Alors le transporteur "TRAN-1" est modifié avec les informations suivantes
      | code   |	nom 		|	prénom	|	adresse                    | téléphone   | email	    | 	siret	        | 
      | TRAN-1 |	MARGUERITE 	|	Claude 	|	2 rue libre 94000 Créteil  | 0715141514  | ch@trans.ifr	| 	82575789690045	| 
