#language: fr

Fonctionnalité: Création d'un transporteur
  En tant qu'administrateur,
  je peux gérer la création d'un transporteur
  afin de gérer le référentiel des transporteurs


  Scénario: Création d'un transporteur avec un profil habilité
    Etant donné je suis un administrateur
    Quand je crée un transporteur avec les informations suivantes
      |	nom 		|	prénom	|	adresse     | téléphone   | email	    | 	siret	        | 
      |	MARGUERITE 	|	Claude 	|	2 rue libre | 0637265485  | ch@trans.io	| 	82575789690023	| 
    Alors un transporteur est créé avec le code "TRAN-1"

  Scénario: Création d'un transporteur avec un profil non habilité
    Etant donné je ne suis pas un administrateur
    Quand je crée un transporteur avec les informations suivantes
      |	nom 		|	prénom	|	adresse     | téléphone   | email	    | 	siret	        | 
      |	MARGUERITE 	|	Claude 	|	2 rue libre | 0637265485  | ch@trans.io	| 	82575789690023	| 
    Alors le système refuse la création du transporteur
