package com.kannacorp.letransporteur.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="Transporteur", uniqueConstraints={@UniqueConstraint(columnNames={"code"})})
@Data
public class Transporteur {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    private String code;
    private String nom;
    @Column(name="prenom")
    private String prénom;
    @Column(name="telephone")
    private String téléphone;
    private String adresse;
    private String email;
    private String siret;
}
