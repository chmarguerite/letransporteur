package com.kannacorp.letransporteur.domaine;

import java.util.List;
import com.kannacorp.letransporteur.model.Transporteur;

public interface TransporteurRepositoryPort {
	List<Transporteur> obtenirTransporteurs();
	Transporteur ajouterTransporteur(Transporteur pTransporteur);
    Transporteur obtenirTransporteurParCode(String pCode);
	Transporteur modifierTransporteur(Transporteur pTransporteur);
	void supprimerTransporteur(String code);

}
