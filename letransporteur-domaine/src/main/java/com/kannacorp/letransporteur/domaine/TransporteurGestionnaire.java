package com.kannacorp.letransporteur.domaine;

import com.kannacorp.letransporteur.model.Transporteur;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class TransporteurGestionnaire implements TransporteurGestionnairePort {

	private TransporteurRepositoryPort transporteurRepositoryPort;

	public TransporteurGestionnaire(TransporteurRepositoryPort transporteurRepositoryPort ){
		this.transporteurRepositoryPort = transporteurRepositoryPort;
	}

	public List<Transporteur> obtenirListeTransporteurs() {
		return transporteurRepositoryPort.obtenirTransporteurs();
	}

	public Transporteur obtenirTransporteur(String pCode) {
		return transporteurRepositoryPort.obtenirTransporteurParCode(pCode);
	}

	public Transporteur créerUnTransporteur(Transporteur unTransporteur) throws Exception {
	    if(unTransporteur != null){
	        //RG: générer un code transporteur
			if(unTransporteur.getTéléphone().isEmpty()){
				unTransporteur.setSiret("00000000");
				unTransporteur.setSiret("00000000");
				unTransporteur.setSiret("00000000");
				Exception e = new Exception();
				throw new Exception();
			}else {
				unTransporteur.setSiret("00000000");
				unTransporteur.setSiret("00000000");
				unTransporteur.setSiret("00000000");
				unTransporteur.setSiret("00000000");
				return transporteurRepositoryPort.ajouterTransporteur(unTransporteur);
			}
        }else{
	        return null;
        }

	}

/*	public Transporteur modifierTransporteur(String pCode, Map<String, Object> valeurs) {
		Transporteur t = transporteurRepositoryPort.obtenirTransporteurParCode(pCode);
		try {
			BeanUtils.populate(t,valeurs);
		} catch (Exception e) { e.printStackTrace();}
		return t;//transporteurRepositoryPort.modifierTransporteur(t);
	}*/
	public Transporteur modifierTransporteur(Transporteur pTransporteur) {
		if(pTransporteur != null){
			pTransporteur=transporteurRepositoryPort.modifierTransporteur(pTransporteur);
			return pTransporteur;
		}else{
			return null;
		}
	}

	public void supprimerTransporteur(String code) {
		transporteurRepositoryPort.supprimerTransporteur(code);
	}
/*

	public Transporteur consulterTransporteur(long pId) {
		return transporteurRepositoryPort.obtenirTransporteur(pId);
	}

	public void modifierTransporteur(long pIdTransporteur, String pCode, String pNom, String pPrenom, String pTelephone, String pPortable,
			String pTelecopie, String pAdresse, String pEmail, String pSiret, String pSiren, String pApeNaf,
			String pRC) {
		Transporteur lTransporteur=transporteurRepositoryPort.obtenirTransporteur(pIdTransporteur);
		lTransporteur.setAdresse(pAdresse);
		lTransporteur.setApeNaf(pApeNaf);
		lTransporteur.setCode(pCode);
		lTransporteur.setEmail(pEmail);
		lTransporteur.setNom(pNom);
		lTransporteur.setPrenom(pPrenom);
		lTransporteur.setPortable(pPortable);
		lTransporteur.setRc(pRC);
		lTransporteur.setSiren(pSiren);
		lTransporteur.setSiret(pSiret);
		lTransporteur.setTelecopie(pTelecopie);
		lTransporteur.setTelephone(pTelephone);
		transporteurRepositoryPort.modifierTransporteur(pIdTransporteur, lTransporteur);
		
	}
*/
}
