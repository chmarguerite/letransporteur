package com.kannacorp.letransporteur.domaine;

import com.kannacorp.letransporteur.model.Transporteur;

import java.util.List;
import java.util.Map;

public interface TransporteurGestionnairePort {

	List<Transporteur> obtenirListeTransporteurs();
	Transporteur obtenirTransporteur(String pCode);
	Transporteur créerUnTransporteur(Transporteur unNouveauTransporteur) throws Exception;
   //Transporteur modifierTransporteur(String code, Map<String, Object> valeurs);
	Transporteur modifierTransporteur(Transporteur pTransporteur);
	void supprimerTransporteur(String code);
	/*
	Transporteur consulterTransporteur(long pId);
	void modifierTransporteur(
				long idTransporteur,
	    		String pCode, 
	    		String pNom,
	    		String pPrenom,
	    		String pTelephone,
	    		String pPortable,
	    		String pTelecopie,
	    		String pAdresse,
	    		String pEmail,
	    		String pSiret,
	    		String pSiren,
	    		String pApeNaf,
	    		String pRC);*/
}
