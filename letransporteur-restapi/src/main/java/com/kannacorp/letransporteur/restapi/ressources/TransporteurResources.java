package com.kannacorp.letransporteur.restapi.ressources;

import java.util.List;

import com.kannacorp.letransporteur.domaine.TransporteurGestionnaire;
import com.kannacorp.letransporteur.domaine.TransporteurGestionnairePort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.kannacorp.letransporteur.domaine.Transporteur;
import org.springframework.web.util.UriComponentsBuilder;


@RestController
@RequestMapping("/api/")
public class TransporteurResources {

	@Autowired(required=true)
	private TransporteurGestionnairePort transporteurService;


	@RequestMapping(value = "/transporteurs", method = RequestMethod.GET)
	public List<Transporteur> obtenirTransporteurs(){
		return transporteurService.obtenirListeTransporteurs();
	}

	@RequestMapping(value = "/transporteurs/{code}", method = RequestMethod.GET)
	public Transporteur obtenirTransporteur(@PathVariable("code") String pCode){
		return transporteurService.obtenirTransporteur(pCode);
	}

	@RequestMapping(value = "/transporteurs", method = RequestMethod.POST)
	public ResponseEntity<?> creerTransporteur(@RequestBody Transporteur transporteur,
											UriComponentsBuilder ucBuilder) {
		Transporteur pformation=transporteurService.créerUnTransporteur(transporteur);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/api/transporteurs/{code}").buildAndExpand(pformation.getCode()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/transporteurs", method = RequestMethod.PUT)
	public ResponseEntity<?> modifierTransporteur(@RequestBody Transporteur transporteur,
											   UriComponentsBuilder ucBuilder) {
		Transporteur pformation=transporteurService.modifierTransporteur(transporteur);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/api/transporteurs/{code}").buildAndExpand(pformation.getCode()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.OK);
	}
}
