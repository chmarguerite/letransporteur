package com.kannacorp.letransporteur.restapi.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan(basePackages = {"com.kannacorp.letransporteur", "com.kannacorp.letransporteur.domaine" })
@EntityScan("com.kannacorp.letransporteur.dao")
@EnableJpaRepositories("com.kannacorp.letransporteur.dao")
public class ApplicationConfig {
}
