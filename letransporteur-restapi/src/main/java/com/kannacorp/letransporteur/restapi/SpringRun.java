package com.kannacorp.letransporteur.restapi;
import com.kannacorp.letransporteur.restapi.configuration.ApplicationConfig;
import com.kannacorp.letransporteur.restapi.configuration.SecurityConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;


@SpringBootApplication
@Import({ApplicationConfig.class,
            SecurityConfig.class })
public class SpringRun  {


    public static void main(String[] args) {
    	
        SpringApplication.run(SpringRun.class, args);
    }

 
}
