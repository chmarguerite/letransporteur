#language: fr

Fonctionnalité: Création d'un transporteur
  En tant qu'administrateur,
  je peux gérer la création d'un transporteur
  afin de gérer le référentiel des transporteurs

  Contexte:
    Etant donné je suis un administrateur

  Scénario: Création d'un transporteur
    Quand je crée un transporteur avec les informations suivantes
      |	nom 		|	prénom	|	adresse     | téléphone   | email	    | 	siret	        | 
      |	MARGUERITE 	|	Claude 	|	2 rue libre 94000 Maisons | 0637265485  | ch@trans.io	| 	82575789690023	| 
    Alors un transporteur est créé avec le code "TRAN-1"


  Scénario: Consultation de la fiche d'un transporteur
    Etant donné le transporteur "TRAN-1" est enregistré
    Quand je recherche le transporteur "TRAN-1"
    Alors j'obtiens la fiche  transporteur suivante
        | Code  |	nom 		|	prénom	|	adresse                   | téléphone   | email	        | 	siret	          |
        |TRAN-1 |	MARGUERITE 	|	Claude 	|	2 rue libre 94000 Maisons | 0637265485  | ch@trans.io	| 	82575789690023    |

  Scénario: Modification d'un transporteur
    Etant donné le transporteur "TRAN-1" est enregistré
    Quand je modifie le transporteur "TRAN-1" avec les informations suivantes
      |	adresse                   | téléphone   | email	        | 	siret	        | 
      |	2 rue libre 94000 Créteil | 0715141514  | ch@trans.ifr	| 	82575789690045	| 
    Alors le transporteur "TRAN-1" est modifié avec les informations suivantes
      | code   |	nom 		|	prénom	|	adresse                    | téléphone   | email	    | 	siret	        | 
      | TRAN-1 |	MARGUERITE 	|	Claude 	|	2 rue libre 94000 Créteil  | 0715141514  | ch@trans.ifr	| 	82575789690045	| 
